In this repro you find sqlite databases with graphs.

You could freely use these files for your research.

Every database has five columns. The name of the graph, the graph itself as a graph6-string, the number of vertices, the number of edges and the coordinates of the vertices.
